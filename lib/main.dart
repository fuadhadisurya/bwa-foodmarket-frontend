import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:luruh_mangan/cubit/cubit.dart';
import 'package:luruh_mangan/models/models.dart';
import 'package:luruh_mangan/ui/pages/pages.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => UserCubit()),
        BlocProvider(create: (_) => FoodCubit()),
        BlocProvider(create: (_) => TransactionCubit()),
      ],
      child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: "Luruh Mangan",
          home: SignInPage()),
    );
  }
}
