part of 'pages.dart';

class PaymentMethodPage extends StatelessWidget {
  final String paymentUrl;

  PaymentMethodPage(this.paymentUrl);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IllustrationPage(
        title: 'Selesaikan Pembayaran',
        subtitle:
        'Silahkan pilih metode pembayaran\nyang telah disediakan',
        picturePath: 'assets/Payment.png',
        buttonTap1: () async {
          await launch(paymentUrl);
        },
        buttonTitle1: 'Metode pembayaran',
        buttonTap2: () {
          Get.to(() => SuccessOrderPage());
        },
        buttonTitle2: 'Lanjutkan',
      ),
    );
  }
}
