part of 'pages.dart';

class SuccessSignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IllustrationPage(
        title: 'Yeah, sudah berhasil',
        subtitle: 'Sekarang kamu sudah bisa pesan makanan',
        picturePath: 'assets/food_wishes.png',
        buttonTap1: () {},
        buttonTitle1: 'Saatnya cari makanan',
      ),
    );
  }
}
