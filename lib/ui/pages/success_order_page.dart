part of 'pages.dart';

class SuccessOrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IllustrationPage(
        title: 'Kamu sudah memesan',
        subtitle:
            'Tunggu dirumah biarkan kami \n mempersiapkan dan mengantarkan \n makanan terbaik kamu',
        picturePath: 'assets/bike.png',
        buttonTap1: () {
          Get.offAll(MainPage());
        },
        buttonTitle1: 'Pesan makanan lain?',
        buttonTap2: () {
          Get.offAll(MainPage(
            initialPage: 1,
          ));
        },
        buttonTitle2: 'Lihat pesanan saya',
      ),
    );
  }
}
