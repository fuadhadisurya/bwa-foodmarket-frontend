part of 'pages.dart';

class AddressPage extends StatefulWidget {
  final User user;
  final String password;
  final File pictureFile;

  AddressPage(this.user, this.password, this.pictureFile);

  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  City selectedCity;
  List<City> cities = [
    City("Indramayu"),
    City("Cirebon"),
    City("Majalengka"),
    City("Kuningan"),
  ];

  List<DropdownMenuItem> generateItems(List<City> cities) {
    List<DropdownMenuItem> items = [];
    for (var item in cities) {
      items.add(DropdownMenuItem(
        child: Text(item.name),
        value: item,
      ));
    }
    return items;
  }

  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController houseNumController = TextEditingController();
  bool isLoading = false;
  List<String> citiess;
  String selectedCityy;

  @override
  void initState() {
    super.initState();

    citiess = ["Indaramayu", "Cirebon", "Majalengka", "Kuningan"];
    selectedCityy = citiess[0];
  }

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: "Alamat",
      subtitle: "Isi alamat dengan benar",
      onBackButtonPressed: () {
        Get.back();
      },
      child: Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
            child: Text(
              "Nomor Handphone",
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              controller: phoneController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: greyFontStyle,
                  hintText: "Isi No Handphone disini"),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              "Alamat",
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              controller: addressController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: greyFontStyle,
                  hintText: "Isi alamat lengkap disini"),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              "Nomor Rumah",
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: TextField(
              controller: houseNumController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: greyFontStyle,
                  hintText: "Isi no rumah disini"),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              "Kota",
              style: blackFontStyle2,
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black)),
            child: DropdownButton(
              hint: Text("Pilih kota"),
              isExpanded: true,
              underline: SizedBox(),
              value: selectedCity,
              items: generateItems(cities),
              onChanged: (item) {
                setState(() {
                  selectedCity = item;
                });
              },
            ),
            // DropdownButton(
            //   value: selectedCity,
            //   hint: Text("Pilih kota"),
            //   isExpanded: true,
            //   underline: SizedBox(),
            //   items: citiess
            //       .map((e) => DropdownMenuItem(
            //           value: e,
            //           child: Text(
            //             e,
            //             style: blackFontStyle3,
            //           )))
            //       .toList(),
            //   onChanged: (item) {
            //     setState(() {
            //       selectedCityy = item;
            //     });
            //   },
            // ),

            //cek 2
            // child: DropdownButton(
            //   hint: Text("Pilih Kota"),
            //   isExpanded: true,
            //   underline: SizedBox(),
            //   items: [
            //     DropdownMenuItem(
            //         child: Text(
            //       "Indramayu",
            //       style: blackFontStyle3,
            //     )),
            //     DropdownMenuItem(
            //         child: Text(
            //       "Cirebon",
            //       style: blackFontStyle3,
            //     )),
            //     DropdownMenuItem(
            //         child: Text(
            //       "Majalengka",
            //       style: blackFontStyle3,
            //     )),
            //     DropdownMenuItem(
            //         child: Text(
            //       "Kuningan",
            //       style: blackFontStyle3,
            //     )),
            //     DropdownMenuItem(
            //         child: Text(
            //       "Subang",
            //       style: blackFontStyle3,
            //     ))
            //   ],
            //   onChanged: (items) {},
            // ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 24),
            height: 45,
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: (isLoading == true)
                ? Center(
                    child: loadingIndicator,
                  )
                : RaisedButton(
                    onPressed: () async {
                      User user = widget.user.copyWith(
                        phoneNumber: phoneController.text,
                        address: addressController.text,
                        houseNumber: houseNumController.text,
                        city: selectedCity.name,
                      );

                      setState(() {
                        isLoading = true;
                      });

                      await context.read<UserCubit>().signUp(
                          user, widget.password,
                          pictureFile: widget.pictureFile);

                      UserState state = context.read<UserCubit>().state;

                      if (state is UserLoaded) {
                        context.read<FoodCubit>().getFoods();
                        context.read<TransactionCubit>().getTransactions();
                        Get.to(() => MainPage());
                      } else {
                        Get.snackbar(
                          "",
                          "",
                          backgroundColor: "D9435E".toColor(),
                          icon: Icon(
                            MdiIcons.closeCircleOutline,
                            color: Colors.white,
                          ),
                          titleText: Text(
                            "Sign Up Gagal",
                            style: GoogleFonts.poppins(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          messageText: Text(
                            (state as UserLoadingFailed).message,
                            style: GoogleFonts.poppins(color: Colors.white),
                          ),
                        );
                        setState(() {
                          isLoading = false;
                        });
                      }
                    },
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    color: mainColor,
                    child: Text(
                      'Daftar sekarang',
                      style: GoogleFonts.poppins(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}

class City {
  String name;

  City(this.name);
}
