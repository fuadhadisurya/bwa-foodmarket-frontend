import 'dart:convert';
import 'dart:io';

import 'package:luruh_mangan/models/models.dart';
import 'package:http/http.dart' as http;

part 'user_services.dart';
part 'food_services.dart';
part 'transaction_services.dart';

String baseURL = 'http://192.168.0.105:8000/api/';
String baseUrlStorage = "http://192.168.0.105:8000/storage/";
